package id.ac.ui.cs.advprog.tutorial4.exercise1;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import id.ac.ui.cs.advprog.tutorial4.exercise1.pizza.CheesePizza;
import id.ac.ui.cs.advprog.tutorial4.exercise1.pizza.ClamPizza;
import id.ac.ui.cs.advprog.tutorial4.exercise1.pizza.Pizza;
import id.ac.ui.cs.advprog.tutorial4.exercise1.pizza.VeggiePizza;

import org.junit.Before;
import org.junit.Test;

public class DepokPizzaStoreTest {
    private DepokPizzaStore depokStore;
    private Pizza pizza;

    @Before
    public void setUp() {
        depokStore = new DepokPizzaStore();
    }

    @Test
    public void testCheesePizza() {
        pizza = depokStore.orderPizza("cheese");
        assertEquals("Depok Style Cheese Pizza", pizza.getName());
        assertTrue(pizza instanceof CheesePizza);
    }

    @Test
    public void testClamPizza() {
        pizza = depokStore.orderPizza("clam");
        assertEquals("Depok Style Clam Pizza", pizza.getName());
        assertTrue(pizza instanceof ClamPizza);
    }

    @Test
    public void testVeggiePizza() {
        pizza = depokStore.orderPizza("veggie");
        assertEquals("Depok Style Veggie Pizza", pizza.getName());
        assertTrue(pizza instanceof VeggiePizza);
    }
}
