package id.ac.ui.cs.advprog.tutorial4.exercise1;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import id.ac.ui.cs.advprog.tutorial4.exercise1.pizza.CheesePizza;
import id.ac.ui.cs.advprog.tutorial4.exercise1.pizza.ClamPizza;
import id.ac.ui.cs.advprog.tutorial4.exercise1.pizza.Pizza;
import id.ac.ui.cs.advprog.tutorial4.exercise1.pizza.VeggiePizza;

import org.junit.Before;
import org.junit.Test;

public class NewYorkPizzaStoreTest {
    private NewYorkPizzaStore nyStore;
    private Pizza pizza;

    @Before
    public void setUp() {
        nyStore = new NewYorkPizzaStore();
    }

    @Test
    public void testCheesePizza() {
        pizza = nyStore.orderPizza("cheese");
        assertEquals("New York Style Cheese Pizza", pizza.getName());
        assertTrue(pizza instanceof  CheesePizza);
    }

    @Test
    public void testClamPizza() {
        pizza = nyStore.orderPizza("clam");
        assertEquals("New York Style Clam Pizza", pizza.getName());
        assertTrue(pizza instanceof  ClamPizza);
    }

    @Test
    public void testVeggiePizza() {
        pizza = nyStore.orderPizza("veggie");
        assertEquals("New York Style Veggie Pizza", pizza.getName());
        assertTrue(pizza instanceof VeggiePizza);
    }
}
