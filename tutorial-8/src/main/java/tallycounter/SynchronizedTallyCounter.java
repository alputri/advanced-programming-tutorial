package tallycounter;

public class SynchronizedTallyCounter {
    private int counter;

    public SynchronizedTallyCounter() {
        this.counter = 0;
    }

    public synchronized void increment() {
        this.counter++;
    }

    public synchronized void decrement() {
        this.counter--;
    }

    public synchronized int value() {
        return this.counter;
    }
}
