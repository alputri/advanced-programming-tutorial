package tallycounter;

import java.util.concurrent.atomic.AtomicInteger;

public class AtomicTallyCounter {
    private AtomicInteger counter;

    public AtomicTallyCounter() {
        this.counter = new AtomicInteger(0);
    }

    public void increment() {
        this.counter.incrementAndGet();
    }

    public void decrement() {
        this.counter.decrementAndGet();
    }

    public int value() {
        return this.counter.get();
    }

}
